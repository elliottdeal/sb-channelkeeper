SB Channelkeeper Data Exploration
=================================

A first pass at checking whether this data might be viable for the upcoming SB R Users' Hackathon.

Data
====
(Goleta is the test case for initial exploration.)

Background
----------
- http://www.sbck.org/current-issues/water-quality-monitoring/download-our-data/
- Data updated through June 2017.
- It's normally released roughly quarterly.
- data dictionary:
    + https://docs.google.com/spreadsheets/d/198Hqj5kB7AHLsy0jUj33UoFcO2t-Tk7ilyeORur1t_o/pub?output=html#
    + This includes parameter descriptions and water quality standards for the central coast and the LA basin
    + edit end of URL to "output=xlsx#" to download an Excel version.
- Master sheet (has site info): https://docs.google.com/spreadsheets/d/1IcXAymbFUAZEmGfU24nO2VOzV682icB-gQQNh3tPiGI/pub?output=xlsx
- Notes from meeting with SB Channelkeeper: https://docs.google.com/document/d/1VhOGE4IV93iX2RE9vAdaprBwLkhm-dsMsdPIASUW3n8/edit



Structure
---------
Each workbook has four sheets:

- Chemistry
- Bacteria
- Nutrients
- Site Conditions

The chemistry, bacteria, and nutrients sheets are in long form, with "ParameterCode" (or "Parameter Code") as the variable column. "Site Conditions" is in semi-long form, with site and date/time as the indices. 


Cleaning
--------
Date and time are split into two columns with inconsistent names across sheets (e.g., "Date", "Time", "SampleDate", "SampleTime", "SampleCollectionTime"). Other column names are also not consistent across sheets.

Missing data--a few columns are quite sparse, most are fully filled or missing a modest amount.

### Dates ###
The sampling isn't completely regular (based on SampleDate/Date). Chemistry and Bacteria run back to 2002, Nutrients to 2000, SiteConditions runs only back to 2014. All data ends on 2017-06-11. For Nutrient, here's also a dropoff around 2012 or so that never really recovers.





Possible questions (from Julien):

- Are sites meeting state water quality objectives? 
- What parameters are primarily contributing to non-compliance?
- Map of good, fair, poor, performance by site
- Where are hot spots for exceedances? 
- Map overlay of agricultural facilities and nitrate exceedances
- Interactive tool for annual Watershed Summary Reports 

My questions/notes:

- Is there any data for Santa Barbara? Perhaps there are not streams?
- The SiteCode/IDs don't seem to match up between the site description sheet and the sampled data.
    + The sampled sites start with "GV" where the site definitions start with "GS".
    + And there are sampled sites that don't appear in the site definition list.
    + Julien's going to ask about this.


Todo
====
- Check whether observations for Goleta apply to other sites.
- Look for patterns in missingness.